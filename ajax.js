
$('#form').on('submit',function(e){
  e.preventDefault();
    var $that = $(this);
  formData = new FormData($that.get(0));
        $.ajax({
          type: 'POST',
          contentType: false,
          processData: false,
          url: 'words.php',
          data: formData,
          success: function(data) {
            $('#results').html(data);
          },
          error:  function(xhr, str){
      alert('Возникла ошибка: ' + xhr.responseCode);
          }
        });
})

$('#results').on('click','a.link',function(e){
  e.preventDefault();
 
  formData = new FormData($('#form').get(0));
        $.ajax({
          type: 'POST',
          contentType: false,
          processData: false,
          url: $(this).attr('href'),
          data: formData,
          success: function(data) {
            $('#results').html(data);
          },
          error:  function(xhr, str){
      alert('Возникла ошибка: ' + xhr.responseCode);
          }
        });
})

 $('select').on('change',function(e){
  if($('select :selected').hasClass('first')){
    $('#menu').addClass('selected');
  }else{
    $('#menu').removeClass('selected');
 }
});

