<?php 

$strings = [];
$strings2 = [];

function dupes($array) {
    $dupe_array = [];
    $result_array = [];
    foreach ($array as $val) {
        if (++$dupe_array[$val] > 1) {
            $result_array[] = $val;
        }
    }
    return $result_array;
}

function viewData($array,$capture = '')
{
	$page = (isset($_GET['page']))? $_GET['page'] : 0; 
	$els = count($array);
	echo "$capture<br>";
	echo '<ul>';
	
	$start = $page*20;
	$last = $start + ((($els - $start) > 20) ? ($start+20) : $els); 
	for($i = $start; $i < $last; $i++)
	{
		if(mb_strlen($array[$i])>0)
			echo "<li>".$array[$i].'</li>';
	}
	echo "</ul>";
	
	if($page > 0)
		echo "<a class = 'link' href ='words.php?page=".($page-1)."' >Пред.</a><br>";
	if(($els -$start) > 20){
		echo "<a class = 'link' href = 'words.php?page=".($page+1)."'>След.</a><br>";
		
	}
	

}

function partedList($array, $count = 1)
{
	$result_array = [];
	for($i = 0; $i < count($array); $i++){
		for($j = 0; $j < count($array); $j++){
			if($i != $j){
			$arr1 = explode(' ',$array[$i]);
			$arr2 = explode(' ', $array[$j]);
			
			$countMerge = count(array_unique(array_merge($arr1,$arr2)));
			$countInter = count(array_intersect($arr1,$arr2));
	
			$diff = abs($countMerge-$countInter);
		
			if($diff <= $count && $countInter > 0){
				$result_array[] = $array[$j];
			}
		}
		}
	}
	
	return $result_array;
}

if(isset($_POST['send'])){
	
	$strings = explode('|',$_POST['words']);
	$type = intval($_POST['type']);

if(isset($_FILES['file']) || !empty($strings)) {
	$storagename = 'base.csv';
	$path = 'upload';
	copy($_FILES["file"]["tmp_name"], $path."/" . $storagename);
	$strings2 = [];
	$f = fopen($path.'/'.$storagename, 'rt') or die('Не могу открыть файл');
	while(($data = fgetcsv($f,1000,';')) !== false)
	{
		$strings2[] = $data[0];
	}
	$result_strings = array_merge($strings,$strings2);
	$result_strings = array_map(function($e){return mb_strtolower(trim($e));}, $result_strings);
	$result_strings2 = dupes($result_strings); 
	if($type == 2){
		$result_strings = partedList($result_strings,intval($_POST['count']));
	}
	$result_strings = array_unique($result_strings);
	fclose($f);
	unlink($path.$storagename);

	viewData($result_strings,'Частичные совпадения');
	viewData($result_strings2,'Полное соотвествие(пересечение)');
}
}
