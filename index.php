<?php 
require_once 'words.php';

 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Тестовое задание</title>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<style type="text/css">
	.selected{
		visibility: hidden;
	}

</style>
</head>
<body>
	<form action="" method="post" enctype="multipart/form-data" id="form">
		<p>Введите ключевые фразы разделененные знаком "|" Также можете подключить файл csv с фразами</p>
		<textarea name="words" wrap="soft" cols = "70" rows="10">
			
		</textarea><br><br>
		<input type="file" name="file"><br><br>
		Тип совпадения:
		<select name="type">
			<option value="1" class="first" >Полное</option>
			<option value="2" >Частичное</option>
		</select>
		<input type="text" name="count" value="1"  id="menu" class="selected">
		<input type="text" name="send" hidden>
		<br><br>
		<input type="submit" name="submit">
	</form>
	<div id="results"></div>
	<script type="text/javascript" src="ajax.js"></script>

</body>
</html>